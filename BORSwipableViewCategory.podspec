
Pod::Spec.new do |s|
  s.name             = "BORSwipableViewCategory"
  s.version          = "0.1.3"
  s.summary          = "UITableViewCell and UIView categories that support swipe to reveal left and right hidden views"
  s.homepage         = "https://bitbucket.org/borlov/borswipableviewcategory"
  s.license          = 'MIT'
  s.author           = { "BohdanOrlov" => "Bohdan.Orlov@gmail.com" }
  s.source           = { :git => "git@bitbucket.org:borlov/borswipableviewcategory.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Classes/**/*.{h,m}'
  s.dependency 'JRSwizzle'
end
