//
//  BORAnySubclassOfTableViewCell.h
//  BORSwipableCellExample
//
//  Created by Bohdan on 5/18/14.
//  Copyright (c) 2014 BOrlov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BORAnySubclassOfTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *customLabel;

@end
