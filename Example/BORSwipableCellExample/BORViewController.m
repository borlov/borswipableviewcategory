//
//  BORViewController.m
//  BORSwipableCellExample
//
//  Created by Bohdan Orlov on 2/18/14.
//  Copyright (c) 2014 BOrlov. All rights reserved.
//

#import "BORViewController.h"
#import "UIView+BORSwipableView.h"
#import "BORAnySubclassOfTableViewCell.h"
#import <BORSwipableViewCategory/UIView+BORSwipableView.h>


static NSString *const swipableCellIdentifier = @"fancyCell";

@interface BORViewController ()

@end

@implementation BORViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    UILabel *leftHiddenView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, self.swipableView.frame.size.height)];
    leftHiddenView.backgroundColor = [UIColor redColor];
    leftHiddenView.text = @"Hi";
    leftHiddenView.textAlignment = NSTextAlignmentCenter;
    UILabel *rightHiddenView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, self.swipableView.frame.size.height)];
    rightHiddenView.backgroundColor = [UIColor blueColor];
    rightHiddenView.text = @"Hello";
    rightHiddenView.textAlignment = NSTextAlignmentCenter;


    self.swipableView.backgroundColor = [UIColor greenColor];
    self.swipableView.BOR_swipable = YES;
    self.swipableView.BOR_swipability.leftHiddenView = leftHiddenView;
    self.swipableView.BOR_swipability.rightHiddenView = rightHiddenView;

//    [self.swipableView removeFromSuperview];


	// Do any additional setup after loading the view, typically from a nib.
}
- (void)viewDidAppear:(BOOL)animated {
    self.swipableView.backgroundColor = [UIColor yellowColor];
//    self.swipableView.swipable = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//#pragma mark UITableView protocols
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BORAnySubclassOfTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:swipableCellIdentifier forIndexPath:indexPath];

    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, tableView.rowHeight)];
    leftButton.backgroundColor = [UIColor redColor];
    [leftButton setTitle:@"Decline" forState:UIControlStateNormal];
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 120, tableView.rowHeight)];
    rightButton.backgroundColor = [UIColor greenColor];
    [rightButton setTitle:@"Accept" forState:UIControlStateNormal];
    cell.contentView.BOR_swipable = YES;
    cell.contentView.BOR_swipability.self.leftHiddenView = leftButton;
    cell.contentView.BOR_swipability.self.rightHiddenView = rightButton;
    cell.contentView.BOR_swipability.self.didRevealHiddenView = ^(UIView *swipableView, UIView *hiddenView) {
        NSLog(swipableView.description);
        NSLog(hiddenView.description);
        NSLog(@"\n");
    };
    cell.contentView.BOR_swipability.keepingRevealed = YES;
    cell.customLabel.text = [NSString stringWithFormat:@"Swipable cell %d", indexPath.row];
//    [cell setButton:leftButton asHiddenViewAtPosition:BORSwipableViewHiddenViewPositionLeft blockAction:^(id <BORSwipableView> buttonOwnerView, UIButton *button) {
//        NSLog(@"Declined");
//    }];
//    [cell setButton:rightButton asHiddenViewAtPosition:BORSwipableViewHiddenViewPositionRight blockAction:^(id <BORSwipableView> buttonOwnerView, UIButton *button) {
//        NSLog(@"Accepted");
//    }];

//    cell.swipableDirections = BORSwipableViewSwipeDirectionBoth;
    return cell;
}
//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Did select");
}
@end
