//
// Created by Bohdan on 6/18/14.
//

#import "UICollectionViewCell+BORSwipableCell.h"
#import <JRSwizzle/JRSwizzle.h>

@implementation UICollectionViewCell (BORSwipableCell)

+ (void)load {
    BOOL success;
    success = [self jr_swizzleMethod:@selector(setSelected:) withMethod:@selector(BOR_setSelected:) error:nil];
    NSParameterAssert(success);
    success = [self jr_swizzleMethod:@selector(setHighlighted:) withMethod:@selector(BOR_setHighlighted:) error:nil];
    NSParameterAssert(success);
    success = [self jr_swizzleMethod:@selector(prepareForReuse) withMethod:@selector(BOR_prepareForReuse) error:nil];
    NSParameterAssert(success);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "InfiniteRecursion"

- (void)BOR_setSelected:(BOOL)selected {
    [self BOR_setSelected:selected];
//    NSLog(@"S %d", selected);
    if (self.contentView.BOR_isSwipable) {
        [self.contentView.BOR_swipability setSelected:selected animated:NO];
        self.contentView.BOR_swipability.cell = self;
    }
}

- (void)BOR_setHighlighted:(BOOL)highlighted {
    [self BOR_setHighlighted:highlighted];
//    NSLog(@"H %d", highlighted);
    if (self.contentView.BOR_isSwipable) {
        [self.contentView.BOR_swipability setHighlighted:highlighted animated:NO];
        self.contentView.BOR_swipability.cell = self;
    }
}

- (void)BOR_prepareForReuse {
    [self BOR_prepareForReuse];
    if (self.contentView.BOR_isSwipable) {
        [self.contentView.BOR_swipability prepareForReuse];
    }
}

@end