//
// Created by Bohdan on 5/17/14.
//

#import "BORSwipability.h"


@interface BORSwipability ()
@property (nonatomic, strong, readwrite) UIView *coverView;
@property (nonatomic, strong, readwrite) UIDynamicAnimator *dynamicAnimator;
@property (nonatomic, weak) UIView *view;
@property (nonatomic) CGRect originalCoverViewFrame;
@property (nonatomic, strong) UIAttachmentBehavior *attachmentBehavior;
@property (nonatomic, strong) UIAttachmentBehavior *panAttachmentBehavior;
@property (nonatomic, strong) UISnapBehavior *snapToRightBehavior;
@property (nonatomic, strong) UISnapBehavior *snapToLeftBehavior;
@property (nonatomic, strong) UISnapBehavior *snapToCenterBehavior;
@property (nonatomic) BOOL rightHiddenViewRevealed;
@property (nonatomic) BOOL leftHiddenViewRevealed;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UIDynamicItemBehavior *dynamicItemBehavior;
@end

@implementation BORSwipability {

}
+ (BORSwipability *)swipabilityForView:(UIView *)view {
    return [[self alloc] initWithView:view];
}

- (instancetype)initWithView:(UIView *)view {
    self = [super init];
    self.view = view;
    self.keepingRevealed = YES;
    return self;
}


- (UIView *)coverView {
    if (_coverView)
        return _coverView;
    _coverView = [self copyViewAndPrepareForBeingCoverView:self.view];
    _coverView.translatesAutoresizingMaskIntoConstraints = YES;
    self.originalCoverViewFrame = _coverView.frame;
    return _coverView;
}


- (void)setLeftHiddenView:(UIView *)leftHiddenView {
    [[self leftHiddenView] removeFromSuperview];
    _leftHiddenView = leftHiddenView;
    CGRect frame = _leftHiddenView.frame;
    frame.origin.x = 0;
    _leftHiddenView.frame = frame;
    [self.view insertSubview:leftHiddenView atIndex:0];
}


- (void)setRightHiddenView:(UIView *)rightHiddenView {
    [[self rightHiddenView] removeFromSuperview];
    _rightHiddenView = rightHiddenView;
    CGRect frame = _rightHiddenView.frame;
    frame.origin.x = self.view.bounds.size.width - frame.size.width;
    _rightHiddenView.frame = frame;
    [self.view insertSubview:rightHiddenView atIndex:0];
}

- (UISnapBehavior *)snapToLeftBehavior {
    if (_snapToLeftBehavior)
        return _snapToLeftBehavior;
    _snapToLeftBehavior = [[UISnapBehavior alloc] initWithItem:self.coverView
        snapToPoint:CGPointMake(-self.rightHiddenView.bounds.size.width + self.view.center.x, self.coverView.center.y)];
    return _snapToLeftBehavior;

}

- (UISnapBehavior *)snapToRightBehavior {
    if (_snapToRightBehavior)
        return _snapToRightBehavior;
    _snapToRightBehavior = [[UISnapBehavior alloc] initWithItem:self.coverView
        snapToPoint:CGPointMake(self.leftHiddenView.bounds.size.width + self.view.center.x, self.coverView.center.y)];
    return _snapToRightBehavior;
}

- (UISnapBehavior *)snapToCenterBehavior {
    if (_snapToCenterBehavior)
        return _snapToCenterBehavior;
    _snapToCenterBehavior = [[UISnapBehavior alloc] initWithItem:self.coverView
        snapToPoint:self.view.center];
    return _snapToCenterBehavior;
}

- (UIDynamicItemBehavior *)dynamicItemBehavior {
    if (_dynamicItemBehavior)
        return _dynamicItemBehavior;
    _dynamicItemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.coverView]];
    _dynamicItemBehavior.allowsRotation = NO;
    return _dynamicItemBehavior;
}

- (UIAttachmentBehavior *)attachmentBehavior {
    if (_attachmentBehavior)
        return _attachmentBehavior;
    _attachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:self.coverView
        attachedToAnchor:self.coverView.center];
    _attachmentBehavior.frequency = 3;
    _attachmentBehavior.damping = 0.9;
    return _attachmentBehavior;
}

- (CGFloat)swipeToRevealVelocityThreshold {
    if (_swipeToRevealVelocityThreshold)
        return _swipeToRevealVelocityThreshold;
    _swipeToRevealVelocityThreshold = 500;
    return _swipeToRevealVelocityThreshold;
}

- (void)setLeftHiddenViewRevealed:(BOOL)leftHiddenViewRevealed {
    _leftHiddenViewRevealed = leftHiddenViewRevealed;
    if (leftHiddenViewRevealed && self.didRevealHiddenView)
        self.didRevealHiddenView(self.view, self.leftHiddenView);
}

- (void)setRightHiddenViewRevealed:(BOOL)rightHiddenViewRevealed {
    _rightHiddenViewRevealed = rightHiddenViewRevealed;
    if (rightHiddenViewRevealed && self.didRevealHiddenView)
        self.didRevealHiddenView(self.view, self.rightHiddenView);
}

- (void)updateTapRecognizer {
    self.tapRecognizer.cancelsTouchesInView = self.leftHiddenViewRevealed || self.rightHiddenViewRevealed;
    [self.coverView removeGestureRecognizer:self.tapRecognizer];
    [self.coverView addGestureRecognizer:self.tapRecognizer];
}

- (UIDynamicAnimator *)dynamicAnimator {
    if (_dynamicAnimator)
        return _dynamicAnimator;
    _dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];

    UIPushBehavior *pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.coverView]
        mode:UIPushBehaviorModeInstantaneous];
    pushBehavior.magnitude = 100;
    pushBehavior.active = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        pushBehavior.active = YES;
    });
    UISnapBehavior *snapBehavior = [[UISnapBehavior alloc] initWithItem:self.coverView
        snapToPoint:self.coverView.center];
//    [animator addBehavior:snapBehavior];

    [_dynamicAnimator addBehavior:self.dynamicItemBehavior];
    [_dynamicAnimator addBehavior:self.snapToCenterBehavior];
//    [_dynamicAnimator addBehavior:self.attachmentBehavior];
//    [_dynamicAnimator addBehavior:pushBehavior];
    [self setDynamicAnimator:_dynamicAnimator];

    return _dynamicAnimator;
}


- (void)makeViewSwipable {
    [self moveSubviewsFromSourceView:self.view toDestinationView:self.coverView];
    [self.view addSubview:self.coverView];
    [self dynamicAnimator];
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
        action:@selector(handlePan:)];
    panRecognizer.delegate = self;
    [self.coverView addGestureRecognizer:panRecognizer];
    panRecognizer.cancelsTouchesInView = YES;
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
        action:@selector(handleTap:)];
    self.tapRecognizer.cancelsTouchesInView = NO;
    [self.coverView addGestureRecognizer:self.tapRecognizer];
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer {
    [self updateTapRecognizer];
    if (self.leftHiddenViewRevealed) {
        self.leftHiddenViewRevealed = NO;
        [self.dynamicAnimator removeBehavior:self.snapToRightBehavior];
        [self.dynamicAnimator addBehavior:self.snapToCenterBehavior];
    }
    else if (self.rightHiddenViewRevealed) {
        self.rightHiddenViewRevealed = NO;
        [self.dynamicAnimator removeBehavior:self.snapToLeftBehavior];
        [self.dynamicAnimator addBehavior:self.snapToCenterBehavior];
    }


}

- (void)handlePan:(UIPanGestureRecognizer *)panGestureRecognizer {
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint translation = [panGestureRecognizer translationInView:self.view];
        if (ABS(translation.x) < ABS(translation.y)) {
            return;
        }

        [self deselectCellIfProvided];
        CGPoint restoredTranslation = CGPointMake(self.coverView.frame.origin.x - self.originalCoverViewFrame.origin.x, self.coverView.frame.origin.y);
        [panGestureRecognizer setTranslation:restoredTranslation inView:self.view];

        [self.dynamicAnimator removeBehavior:self.snapToLeftBehavior];
        [self.dynamicAnimator removeBehavior:self.snapToRightBehavior];
        [self.dynamicAnimator removeBehavior:self.snapToCenterBehavior];
//        [self.dynamicAnimator removeBehavior:self.attachmentBehavior];
//        self.attachmentBehavior = nil;
//        NSLog(@"set dX %f", restoredTranslation.x);


        CGPoint newAnchorPoint = CGPointMake(self.view.center.x + restoredTranslation.x, panGestureRecognizer.view.center.y);
        self.panAttachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:panGestureRecognizer.view
            attachedToAnchor:newAnchorPoint];
        self.panAttachmentBehavior.damping = 1;
        [self.dynamicAnimator addBehavior:self.panAttachmentBehavior];
    }

    CGPoint translation = [panGestureRecognizer translationInView:self.view];
    CGFloat translationX = translation.x;
//    NSLog(@"dX %f", translation);
    if (translationX > 0)
        translationX = MIN(self.leftHiddenView.bounds.size.width, translationX);
    else
        translationX = MAX(-self.rightHiddenView.bounds.size.width, translationX);
//    CGPoint newAnchorPoint = CGPointMake(newX, panGestureRecognizer.view.center.y);
    CGPoint newAnchorPoint = CGPointMake(self.view.center.x + translationX, panGestureRecognizer.view.center.y);
    self.panAttachmentBehavior.anchorPoint = newAnchorPoint;
//        self.originalCoverViewFrame = self.coverView.frame;

//    self.coverView.frame = CGRectOffset(self.originalCoverViewFrame, dx, 0);
    if (panGestureRecognizer.state == UIGestureRecognizerStateEnded || panGestureRecognizer.state == UIGestureRecognizerStateCancelled) {

        CGPoint velocity = [panGestureRecognizer velocityInView:self.view];
        if (self.keepingRevealed) {
            if (translationX >= self.leftHiddenView.bounds.size.width || (velocity.x > self.swipeToRevealVelocityThreshold && !self.rightHiddenViewRevealed)) {
                [self.dynamicAnimator addBehavior:self.snapToRightBehavior];
                self.rightHiddenViewRevealed = NO;
                self.leftHiddenViewRevealed = YES;
//            self.attachmentBehavior.anchorPoint = CGPointMake(self.leftHiddenView.bounds.size.width + self.view.center.x , self.coverView.center.y);
            }
            else if (translationX <= -self.rightHiddenView.bounds.size.width || (velocity.x < -self.swipeToRevealVelocityThreshold && !self.leftHiddenViewRevealed)) {
                [self.dynamicAnimator addBehavior:self.snapToLeftBehavior];
                self.leftHiddenViewRevealed = NO;
                self.rightHiddenViewRevealed = YES;
//            self.attachmentBehavior.anchorPoint = CGPointMake(- self.rightHiddenView.bounds.size.width + self.view.center.x , self.coverView.center.y);
            }
            else {
                self.leftHiddenViewRevealed = NO;
                self.rightHiddenViewRevealed = NO;
//            self.attachmentBehavior = nil;
//            [self.dynamicAnimator addBehavior:self.attachmentBehavior];
//            self.attachmentBehavior.anchorPoint = self.view.center;
                [self.dynamicAnimator addBehavior:self.snapToCenterBehavior];
            }
        }
        else {
            if (!self.panAttachmentBehavior.dynamicAnimator)
                return;

            if (translationX >= self.leftHiddenView.bounds.size.width || (velocity.x > self.swipeToRevealVelocityThreshold && !self.rightHiddenViewRevealed)) {
                self.leftHiddenViewRevealed = YES;
            }
            else if (translationX <= -self.rightHiddenView.bounds.size.width || (velocity.x < -self.swipeToRevealVelocityThreshold && !self.leftHiddenViewRevealed)) {
                self.rightHiddenViewRevealed = YES;
            }
            self.leftHiddenViewRevealed = NO;
            self.rightHiddenViewRevealed = NO;
//            self.attachmentBehavior = nil;
//            [self.dynamicAnimator addBehavior:self.attachmentBehavior];
//            self.attachmentBehavior.anchorPoint = self.view.center;
            [self.dynamicAnimator addBehavior:self.snapToCenterBehavior];

        }


        [self.dynamicAnimator removeBehavior:self.panAttachmentBehavior];


//        self.attachmentBehavior.anchorPoint = CGPointZero;
//        self.attachmentBehavior.anchorPoint = self.view.center;
//        [self.dynamicAnimator removeBehavior:self.attachmentBehavior];
//        self.attachmentBehavior.length = ABS(dx);
//        [self.dynamicAnimator addBehavior:self.attachmentBehavior];
//        self.attachmentBehavior.length = 0;
    }
}

- (void)deselectCellIfProvided {
    id view = [self.cell superview];

    while (view && ![view isKindOfClass:[UITableView class]])
        view = [view superview];

    UITableView *tableView = (UITableView *) view;
    [tableView deselectRowAtIndexPath:[tableView indexPathForCell:self.cell] animated:NO];
}

- (void)makeViewNotSwipable {
    [self moveSubviewsFromSourceView:self.coverView toDestinationView:self.view];
    [self.coverView removeFromSuperview];
    self.coverView = nil;
}

- (void)moveSubviewsFromSourceView:(UIView *)sourceView toDestinationView:(UIView *)destinationView {
    NSArray *constraints = [sourceView constraints];

    NSMutableArray *newConstraints = [NSMutableArray array];

    for (UIView *view in sourceView.subviews) {
        //TODO make viewsToIgnore array
        if (view == self.view || view == destinationView || view == self.leftHiddenView || view == self.rightHiddenView) {
            continue;
        }

        for (NSLayoutConstraint *constraint in constraints) {

            UIView *firstItem = (UIView *) constraint.firstItem;
            UIView *secondItem = (UIView *) constraint.secondItem;

            //if view was added without attributes it generates constraint without second item, probably useless
            if (!firstItem || !secondItem)
                continue;

            if (firstItem == sourceView) {
                firstItem = destinationView;
            }

            if (secondItem == sourceView) {
                secondItem = destinationView;
            }

            NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:firstItem
                attribute:constraint.firstAttribute
                relatedBy:constraint.relation
                toItem:secondItem
                attribute:constraint.secondAttribute
                multiplier:constraint.multiplier
                constant:constraint.constant];
            newConstraint.priority = constraint.priority;
            [newConstraints addObject:newConstraint];
        }

        [view removeFromSuperview];
        [destinationView addSubview:view];

    }

    if (newConstraints.count) {
        [destinationView addConstraints:newConstraints];
    }
}

- (UIView *)copyView:(UIView *)view {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:view]];
}

- (UIView *)copyViewAndPrepareForBeingCoverView:(UIView *)view {
    UIView *uiView = [self copyView:view];
    [uiView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGRect frame = uiView.frame;
    frame.origin = CGPointZero;
    uiView.frame = frame;
    return uiView;
}

- (void)layoutSubviews {
    [self moveSubviewsFromSourceView:self.view toDestinationView:self.coverView];
}

- (void)setBackgroundColor:(UIColor *)color {
    self.coverView.backgroundColor = color;
}

- (BOOL)                         gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//    CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self.view];
//    if (ABS(translation.x) > ABS(translation.y))
//        [gestureRecognizer requireGestureRecognizerToFail:<#(UIGestureRecognizer *)otherGestureRecognizer#>] = YES;
//    else
//        gestureRecognizer.enabled = NO;
    return YES;
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//        CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self.view];
//    if (ABS(translation.x) > ABS(translation.y))
//        return NO;
//    return YES;
//}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    self.leftHiddenView.hidden = selected;
    self.rightHiddenView.hidden = selected;

}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    self.leftHiddenView.hidden = highlighted || [self.cell isSelected];
    self.rightHiddenView.hidden = highlighted || [self.cell isSelected];

}

- (void)prepareForReuse {
    [self coverHiddenViewsAnimated:NO];

}

- (void)coverHiddenViewsAnimated:(BOOL)animated {
    self.leftHiddenViewRevealed = NO;
    self.rightHiddenViewRevealed = NO;
    [self.dynamicAnimator removeBehavior:self.snapToLeftBehavior];
    [self.dynamicAnimator removeBehavior:self.snapToRightBehavior];
    if (animated)
        [self.dynamicAnimator addBehavior:self.snapToCenterBehavior];
    else {

        [self.dynamicAnimator removeBehavior:self.dynamicItemBehavior];
        [self.dynamicAnimator removeBehavior:self.snapToCenterBehavior];
        self.coverView.frame = self.originalCoverViewFrame;
        [self.dynamicAnimator addBehavior:self.dynamicItemBehavior];
        [self.dynamicAnimator addBehavior:self.snapToCenterBehavior];
    }

}
@end