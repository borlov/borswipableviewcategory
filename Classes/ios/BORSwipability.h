//
// Created by Bohdan on 5/17/14.
//

#import <Foundation/Foundation.h>

NS_OPTIONS(NSInteger, BORSwipableViewSwipeDirections){
    BORSwipableViewSwipeDirectionNone = 1 << 0,
    BORSwipableViewSwipeDirectionLeft = 1 << 1,
    BORSwipableViewSwipeDirectionRight = 1 << 2,
    BORSwipableViewSwipeDirectionBoth = ~0
};

@interface BORSwipability : NSObject <UIGestureRecognizerDelegate>
@property (nonatomic, getter=isKeepingRevealed) BOOL keepingRevealed;
@property (nonatomic) CGFloat swipeToRevealVelocityThreshold;
//@property (nonatomic) enum BORSwipableViewSwipeDirections swipableDirections;
@property (nonatomic, strong, readonly) UIView *coverView;
@property (nonatomic, strong, readonly) UIDynamicAnimator *dynamicAnimator;
@property (nonatomic, strong) UIView *leftHiddenView;
@property (nonatomic, strong) UIView *rightHiddenView;
@property (nonatomic, strong) id cell;
@property (nonatomic, copy) void (^didRevealHiddenView)(UIView *swipableView, UIView *hiddenView);
+ (BORSwipability *)swipabilityForView:(UIView *)view;
- (instancetype)initWithView:(UIView *)view;
- (void)makeViewSwipable;
- (void)makeViewNotSwipable;
- (void)layoutSubviews;
- (void)setBackgroundColor:(UIColor *)color;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated;
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated;
- (void)prepareForReuse;
@end