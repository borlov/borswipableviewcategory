//
// Created by Bohdan on 5/18/14.
//

#import "UITableViewCell+BORSwipableCell.h"
#import <JRSwizzle/JRSwizzle.h>


@implementation UITableViewCell (BORSwipableCell)

+ (void)load {
    BOOL success;
    success = [self jr_swizzleMethod:@selector(setSelected:animated:)
        withMethod:@selector(BOR_setSelected:animated:) error:nil];
    NSParameterAssert(success);
    success = [self jr_swizzleMethod:@selector(setHighlighted:animated:)
        withMethod:@selector(BOR_setHighlighted:animated:) error:nil];
    NSParameterAssert(success);
    success = [self jr_swizzleMethod:@selector(prepareForReuse)
        withMethod:@selector(BOR_prepareForReuse) error:nil];
    NSParameterAssert(success);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "InfiniteRecursion"
- (void)BOR_setSelected:(BOOL)selected animated:(BOOL)animated {
    [self BOR_setSelected:selected animated:animated];
//    NSLog(@"S %d", selected);
    if (self.contentView.BOR_isSwipable) {
        [self.contentView.BOR_swipability setSelected:selected animated:animated];
        self.contentView.BOR_swipability.cell = self;
    }
}
- (void)BOR_setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [self BOR_setHighlighted:highlighted animated:animated];
//    NSLog(@"H %d", highlighted);
    if (self.contentView.BOR_isSwipable){
        [self.contentView.BOR_swipability setHighlighted:highlighted animated:animated];
        self.contentView.BOR_swipability.cell = self;
    }
}
- (void)BOR_prepareForReuse {
    [self BOR_prepareForReuse];
    if (self.contentView.BOR_isSwipable){
        [self.contentView.BOR_swipability prepareForReuse];
    }
}

#pragma clang diagnostic pop
@end