//
// Created by Bohdan on 5/14/14.
//

#import "UIView+BORSwipableView.h"
#import <objc/runtime.h>
#import <JRSwizzle/JRSwizzle.h>

@interface UIView ()
@property (nonatomic, strong, readwrite) BORSwipability *BOR_swipability;
@end

@implementation UIView (BORSwipableView)
+ (void)load {
    BOOL success;
    success = [self jr_swizzleMethod:@selector(layoutSubviews) withMethod:@selector(BOR_layoutSubviews)
        error:nil];
    NSParameterAssert(success);
    success = [self jr_swizzleMethod:@selector(setBackgroundColor:)
        withMethod:@selector(BOR_setBackgroundColor:) error:nil];
    NSParameterAssert(success);
}

- (BOOL)BOR_isSwipable {
    return [objc_getAssociatedObject(self, @selector(BOR_isSwipable)) boolValue];
}

- (void)setBOR_swipable:(BOOL)swipable {
    if (self.BOR_isSwipable == swipable)
        return;
    objc_setAssociatedObject(self, @selector(BOR_isSwipable), @(swipable), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (swipable)
        [self.BOR_swipability makeViewSwipable];
    else
        [self.BOR_swipability makeViewNotSwipable];
}

- (BORSwipability *)BOR_swipability {
    if (!self.BOR_isSwipable)
        return nil;
    BORSwipability *swipability = objc_getAssociatedObject(self, @selector(BOR_swipability));
    if (swipability)
        return swipability;
    swipability = [BORSwipability swipabilityForView:self];
    objc_setAssociatedObject(self, @selector(BOR_swipability), swipability, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return swipability;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "InfiniteRecursion"
- (void)BOR_layoutSubviews {

    [self BOR_layoutSubviews];
    if (self.BOR_isSwipable)
        [self.BOR_swipability layoutSubviews];
}

- (void)BOR_setBackgroundColor:(UIColor *)backgroundColor {
    [self BOR_setBackgroundColor:backgroundColor];
    if (self.BOR_isSwipable)
        [self.BOR_swipability setBackgroundColor:backgroundColor];
}

#pragma clang diagnostic pop

@end