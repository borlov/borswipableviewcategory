//
// Created by Bohdan on 5/14/14.
//

#import <Foundation/Foundation.h>
#import "BORSwipability.h"

@interface UIView (BORSwipableView)
@property (nonatomic, getter=BOR_isSwipable) BOOL BOR_swipable;
@property (nonatomic, strong, readonly) BORSwipability *BOR_swipability;
@end